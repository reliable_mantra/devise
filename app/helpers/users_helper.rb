module UsersHelper
  def ban_status(user)
    user.access_locked? ? 'Unban' : 'Ban'
  end
end
